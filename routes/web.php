<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/blog', function () {
    return view('blog');
});
Route::get('/booking', function () {
    return view('booking');
});
Route::get('/services', function () {
    return view('services');
});
Route::get('/servicedetail', function () {
    return view('servicedetail');
});
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/singlepost', function () {
    return view('singlepost');
});