<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Hire A Helper</title>
<!-- Bootstrap -->
<script src="https://use.fontawesome.com/ded002a609.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link rel="stylesheet" href="css/font-awesome.min.css" />
<link rel="stylesheet" href="css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" href="css/owlcarousel/owl.theme.default.min.css" />
<link  href="css/datetime/bootstrap-datetimepicker.css" rel="stylesheet" />
</head>

<body>
<div id="wrapper" class="inner_page booking_page"><!--Wrapper Start--> 
  <!--Header Section Start-->
  <header id= "header" data-spy="affix" data-offset-top="60" data-offset-bottom="60">
    <div class="container">
      <div class="row">
        <div class="col-md-8  col-sm-12 col-xs-12 col-sm-12">
          <nav class="navbar"> 
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="/"><img class="logo-dark hidden-xs"  src="images/logo.png" alt="" /> <img class="logo-dark hidden-lg hidden-md hidden-sm"  src="images/mobile_logo.png" alt="" /></a> </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="main-menu collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="/"> Home </a>
                    </li>
                  <li><a href="/about">About</a></li>
                  <li class="active"><a href="/services">Service</a></li>
                  <li role="presentation" class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> Pages <span class="caret"></span> </a>
                    <ul class="dropdown-menu">
                      <li><a href="/booking">Booking Page</a></li>
                      <li><a href="/servicedetail">Servise Deatil</a></li>
                      <li><a href="/blog">Blog</a></li>
                      <li><a href="/singlepost">Single Post</a></li>
                    </ul>
                  </li>
                  <li><a href="/contact">Contact</a></li>
                </ul>
              </div>
            <!-- /.navbar-collapse --> 
          </nav>
        </div>
        <div class="col-md-4  col-sm-12 col-xs-12 col-sm-12 hidden-xs">
          <ul class="right-contact">
            <li><i class="fa fa-phone" aria-hidden="true"></i> +91 8107186985</li>
            <li><a href="#" class="btn btn-primary btn-skin">Get Free Quote</a></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /.container --> 
  </header>
  <!--/Header Section End--> 
  <!--Page Title Section Satrt-->
  <div id="page_title">
    <div class="container text-center">
      <div class="panel-heading">book now</div>
      <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active">Book Now</li>
      </ol>
    </div>
  </div>
  <!--Page Title Section End--> 
  
  <!--Contact Information Start-->
  <section id="contact_information">
    <div class="container">
      <div class="row"> 
        <!--Left Form Part-->
        <div class="col-md-8 col-sm-8 col-xs-12"> 
          
          <!--Contact Information-->
          <div class="contact_information_left "> 
            
            <!-- HTML Form (wrapped in a .bootstrap-iso div) -->
            <div class="booking_form">
              <div class="container-fluid">
                <div class="row">
                  <form method="post">
                    <h2>Contact Information</h2>
                    <p>This information will be used to contact you about your service</p>
                    <div class="form-group col-md-12">
                      <input class="form-control" id="name" name="name" placeholder="Full Name" type="text"/>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                      <input class="form-control" id="email" name="email" placeholder="Email*" type="text"/>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-r">
                      <input class="form-control" id="tel" name="tel" placeholder="Phone Number*" type="text"/>
                    </div>
                    <div class="clearfix"></div>
                    <hr />
                    <!--Service Address-->
                    <h2>Service Address</h2>
                    <p>Where would you like us to clean?</p>
                    <div class="form-group col-lg-12 ">
                      <input class="form-control" id="address" name="address" placeholder="Address*" type="text"/>
                    </div>
                    
                    <div class="clearfix"></div>
                    
                    
                    <hr />
                    <h2>Picture Upload</h2>
                    <p>Upload a picture of the item to be fixed</p>
                    <div class="form-group col-lg-12 ">
                      <input class="form-control" type="file" class="form-control"/>
                    </div>
                    
                    <div class="clearfix"></div>
                    <hr />
                    <!--Select Date Time-->
                    <h2>When would you like us to come?</h2>
                    <p></p>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                      <div class='input-group date' id='datetimepicker1'>
                        <input type='text' class="form-control" />
                        <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12 padding-r">
                      <div class='input-group date' id='datetimepicker3'>
                        <input type='text' class="form-control" />
                        <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span> </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr />
                    <!--How often?-->
                    
                    <div class="clearfix"></div>
            
                    
                    <!--BOOK NOW-->
                    
                    <!-- <div class="booking_summary hidden-lg  hidden-md hidden-sm">
                      <h1>Booking Summary</h1>
                      <ul>
                        <li><i class="fa fa-home" aria-hidden="true"></i>Home Cleaning</li>
                        <li><i class="fa fa-calendar" aria-hidden="true"></i>Cleaning date</li>
                        <li><i class="fa fa-refresh" aria-hidden="true"></i>Every 2 weeks</li>
                      </ul>
                      <div class="price_totle">
                        <div class="subtotal">
                          <div class="heading text-left">SUBTOTAL</div>
                          <div class="price text-right">$119.00</div>
                        </div>
                        <div class="subtotal">
                          <div class="heading text-left">DISCOUNT</div>
                          <div class="price text-right">$17.85</div>
                        </div>
                        <div class="subtotal">
                          <div class="heading text-left">TOTAL:</div>
                          <div class="price text-right">$101.15</div>
                        </div>
                      </div>
                    </div> -->
                    <p>By clicking the Book Now button you are agreeing to our Terms of Service and Privacy Policy.</p>
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                      <button class="btn btn-primary btn-skin" name="submit" type="submit"> REQUEST NOW</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <!--Contact Information--> 
          
        </div>
        <!--/Left Form Part-->
        
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="contact_information_right text-center">
            <div class="booking_summary">
              <div class="icon_box_one">
                <div class="icons"><img src="images/booking/time3.png" alt="time3" /></div>
                <div class="box_content">
                  <h4>SAVES YOU TIME</h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                </div>
              </div>
              <div class="icon_box_one">
                <div class="icons"><img src="images/booking/Safety3.png" alt="Safety3" /></div>
                <div class="box_content">
                  <h4>For Your Safety</h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                </div>
              </div>
              <div class="icon_box_one">
                <div class="icons"><img src="images/booking/best3.png" alt="best3" /></div>
                <div class="box_content">
                  <h4>Best-Rated Professionals</h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                </div>
              </div>
              <div class="icon_box_one">
                <div class="icons"><img src="images/booking/Equipped3.png" alt="Equipped3" /></div>
                <div class="box_content">
                  <h4>We Are Well Equipped</h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                </div>
              </div>
              <div class="icon_box_one">
                <div class="icons"><img src="images/booking/touch3.png" alt="touch3" /></div>
                <div class="box_content">
                  <h4>Always In Touch</h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                </div>
              </div>
              <div class="icon_box_one">
                <div class="icons"><img src="images/booking/cash3.png" alt="cash3" /></div>
                <div class="box_content">
                  <h4>Cash-Free Facility</h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                </div>
              </div>
              <div class="box_btn">
                <button class="btn btn-primary booknow btn-skin" type="submit">LEARN MORE</button>
              </div>
            </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--Contact Information End--> 
  
  <!--Footer-->
  <footer>
    <div class="container-fluid footerbg">
      <div class="container">
        <div class="row">
          <div class="col-md-3"> <a href="#" class="footer-logo"> <img class="logo-dark"  src="images/logo2.png" alt="logo2" /> </a>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            <div class="about_info">
              <p><i class="fa fa-map-marker" aria-hidden="true"></i> Lorem Ipsum is simply dummy</p>
              <p><i class="fa fa-envelope" aria-hidden="true"></i> infor@example.com</p>
              <p><i class="fa fa-phone" aria-hidden="true"></i> +91807186985</p>
            </div>
          </div>
          <div class="col-md-3">
            <h4>Services</h4>
            <ul>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Cleaning</a></li>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Electrical</a></li>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Plumbing</a></li>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Appliances</a></li>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Carpentry</a></li>
            </ul>
            <ul>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Geyser Service</a></li>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Vehicle Care</a></li>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Pest Control</a></li>
              <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Panting</a></li>
            </ul>
          </div>
          <div class="col-md-2">
            <h4>About Us</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">About us</a></li>
              <li><a href="#">Services</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </div>
          <div class="col-md-4">
            <h4>Singn up Newsletter</h4>
            <form action="#" method="post" class="newsletter">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Enter Email Address" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                </span> </div>
              <!-- /input-group -->
            </form>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
          </div>
        </div>
        <div class="top_awro pull-right" id="back-to-top"><i class="fa fa-chevron-up" aria-hidden="true"></i> </div>
      </div>
    </div>
    
    <!--Boottom Footer-->
    <div class="container-fluid bottom-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <p class="copyright pull-left">&copy; Hire A Helper 2017 All Right Reserved</p>
            <ul class="footer-scoails pull-right">
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--/Footer--> 
  
</div>
<!--/Wrapper End--> 

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 

<script src="js/bootstrap.min.js"></script> 
<script  src="js/datetime/moment.js"></script> 
<script  src="js/datetime/bootstrap-datetimepicker.min.js"></script> 
<script src="js/owlcarousel/owl.carousel.min.js"></script> 
<script src="js/custom.js"></script> 
<script type="application/javascript">
$(document).ready(function(){
    
	$(function () {
                $('#datetimepicker1').datetimepicker();
            });
       
            $(function () {
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
            });	
		
});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-106074231-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
  function activatePlacesSearch(){
    var input = document.getElementById('address');
    var autocomplete = new google.maps.places.Autocomplete(input);
  }
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFobcWVCn4Up3j2TZvKxOkB_9ClAh5JO4&libraries=places&callback=activatePlacesSearch"></script>
</body>
</html>